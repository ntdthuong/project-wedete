module.exports = {
  autoPrefixBrowserList: ['last 2 version'],
  paths: {
    dest: 'dist/',
    data: {
      src: 'app/data/*',
      dest: 'dist/data'
    },
    media: {
      src: 'app/media/*',
      dest: 'dist/media'
    },
    fonts: {
      src: 'app/fonts/*',
      dest: 'dist/fonts'
    },
    iconfont: {
      fontPath: '../fonts/',
      iconTemplate: 'gulp/template/_icons.scss',
      src: 'app/iconfont/*',
      templateDest: 'app/styles/_components',
      dest: 'dist/fonts'
    },
    images: {
      src: 'app/images/*',
      dest: 'dist/images'
    },
    styles: {
      all: ['app/styles/*.scss', 'app/styles/**/*.scss'],
      include: 'app/styles/',
      lint: ['app/styles/*.scss', 
        'app/styles/**/*.scss', 
        '!app/styles/_vendors/*.scss',
        '!app/styles/_base/_reset.scss'
      ],
      src: 'app/styles/*.scss',
      dest: 'dist/styles'
    },
    scripts: {
      dest: 'dist/scripts',
      src: ['app/scripts/*.js', 'app/scripts/_plugins/*.js', 'app/scripts/_pages/*.js'],
      vendorSrc: ['app/scripts/_vendors/*.js', 'app/scripts/_vendors/**/*.js'],
      watch: ['app/scripts/*.js', 'app/scripts/**/*.js']
    },
    views: {
      all: ['app/views/*.pug', 'app/views/**/*.pug'],
      src: 'app/views/*.pug',
      dest: 'dist'
    },
    ghPages: {
      src: './'
    }
  },
  names: {
    iconfont: 'icons',
    css: 'app.min.css',
    js: {
      app: 'app.js',
      appMin: 'app.min.js',
      vendor: 'vendor.js',
      vendorMin: 'vendor.min.js'
    }
  }
};