const config = require('../config');
const del    = require('del');
const gulp   = require('gulp');

gulp.task('clean', () => {
  del.sync([
    config.paths.dest
  ]);
});