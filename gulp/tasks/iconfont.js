const async        = require('async');
const config       = require('../config');
const consolidate  = require('gulp-consolidate');
const fontName     = config.names.iconfont;
const gulp         = require('gulp');
const handleErrors = require('../helpers/handle-errors');
const iconfont     = require('gulp-iconfont');
const iconfontPath = config.paths.iconfont;
const plumber      = require('gulp-plumber');

gulp.task('iconfont',  (done) => {
  const iconStream = gulp.src(iconfontPath.src)
    .pipe(plumber({ errorHandler: handleErrors }))
    .pipe(iconfont({ fontName: fontName }));
  async.parallel([
    function handleGlyphs(cb) {
      iconStream.on('glyphs', function (glyphs, options) {
        gulp.src(iconfontPath.iconTemplate)
          .pipe(consolidate('lodash', {
            glyphs: glyphs,
            fontName: fontName,
            fontPath: iconfontPath.fontPath,
            fontHeight: 1001,
            className: 'icon'
          }))
          .pipe(gulp.dest(iconfontPath.templateDest))
          .on('finish', cb);
      });
    },
    function handleFonts(cb) {
      iconStream
        .pipe(gulp.dest(iconfontPath.dest))
        .on('finish', cb);
    }
  ], done);
});