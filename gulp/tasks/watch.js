const cached      = require('gulp-cached');
const config      = require('../config');
const gulp        = require('gulp');
const paths       = config.paths;
const remember    = require('gulp-remember');
const runSequence = require('run-sequence');

gulp.task('watch:dev', () => {
  gulp.watch(paths.data.src, ['data:copy']);
  gulp.watch(paths.fonts.src, ['fonts:copy']);
  gulp.watch(paths.media.src, ['media:copy']);
  gulp.watch(paths.images.src, ['imagemin']);
  gulp.watch(paths.iconfont.src, ['iconfont']);
  gulp.watch(paths.scripts.watch, ['scripts:dev']).on('change', function (event) {
    if (event.type === 'deleted') {
      delete cached.caches['scripts'][event.path];
      remember.forget('scripts', event.path);
    }
  });
  gulp.watch(paths.styles.all, ['styles:dev']).on('change', function (event) {
    if (event.type === 'deleted') {
      delete cached.caches['stylesLint'][event.path];
    }
  });
  gulp.watch(paths.views.all, ['views:dev']).on('change', function (event) {
    if (event.type === 'deleted') {
      delete cached.caches['pug'][event.path];
    }
  });
});